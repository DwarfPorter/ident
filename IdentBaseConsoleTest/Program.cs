﻿using IdentBase.Context;
using System;

namespace IdentBaseConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());

            using (DataOperation dbo = new DataOperation())
            {
                foreach (var item in dbo.GetEmployees())
                {
                    Console.WriteLine(item.Id + " | " + item.Inn + " | " + item.Name);
                }

            }



            Console.WriteLine("Press Enter");
            Console.ReadLine();

        }
    }
}
