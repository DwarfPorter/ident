﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentBase.Models
{
    public class Employee
    {
        private DbModels.Employee employee;
        private DbModels.Person person;

        public int Id
        {
            get
            {
                return employee.Id;
            }
        }
        public string Inn
        {
            get
            {
                return employee.Inn;
            }
        }
        public string Name
        {
            get
            {
                return string.Format("{0} {1} {2}", person.Name, person.ParentalName, person.Surname);
            }
        }
        public DbModels.Employee SetEmployee
        {
            set
            {
                employee = value;
            }
        }
        public DbModels.Person SetPerson
        {
            set
            {
                person = value;
            }
        }


    }
}
