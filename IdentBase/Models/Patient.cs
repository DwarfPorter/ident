﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentBase.Models
{
    public class Patient
    {
        private DbModels.Patient patient;
        private DbModels.Person person;

        public int Id
        {
            get
            {
                return patient.Id;
            }
        }
        public string NumberMedcard
        {
            get
            {
                return patient.NumberMedcard;
            }
        }
        public string Name
        {
            get
            {
                return string.Format("{0} {1} {2}", person.Name, person.ParentalName, person.Surname);
            }
        }
        public DbModels.Patient SetPatient
        {
            set
            {
                patient = value;
            }
        }
        public DbModels.Person SetPerson
        {
            set
            {
                person = value;
            }
        }
    }
}
