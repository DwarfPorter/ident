﻿using System.Linq;
using IdentBase.Models;
using System;

namespace IdentBase.Context
{
    public class DataOperation : IDataOperation, IDisposable
    {
        private IdentContext db;

        public DataOperation()
        {
            db = new IdentContext();
        }

        public void Dispose()
        {
            if (db != null)
                db.Dispose();
        }

        public IQueryable<Employee> GetEmployees()
        {
            return from e in db.Employees
                   join p in db.People on e.Id equals p.Id
                   select new Employee { SetEmployee = e, SetPerson = p };
        }

        public IQueryable<Patient> GetPatients()
        {
            return from pt in db.Patients
                   join p in db.People on pt.Id equals p.Id
                   select new Patient { SetPatient = pt, SetPerson = p };
        }
    }
}
