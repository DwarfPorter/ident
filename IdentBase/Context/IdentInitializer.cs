﻿using IdentBase.DbModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentBase.Context
{
    public partial class IdentInitializer : CreateDatabaseIfNotExists<IdentContext>
    {
        protected override void Seed(IdentContext context)
        {
            for (var i = 0; i < 100; i++)
            {
                var personInitializer = new PersonInitializer();
                personInitializer.Build();
                var person = personInitializer.Person;
                var employee = personInitializer.Employee;
                var patient = personInitializer.Patient;
                context.People.Add(person);
                if (employee != null) context.Employees.Add(employee);
                if (patient != null) context.Patients.Add(patient);
            }
            base.Seed(context);
        }
    }
}
