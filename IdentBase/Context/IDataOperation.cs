﻿using System.Linq;

namespace IdentBase.Context
{
    public interface IDataOperation
    {
        IQueryable<Models.Employee> GetEmployees();
        IQueryable<Models.Patient> GetPatients();
    }
}
