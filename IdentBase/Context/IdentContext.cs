﻿using IdentBase.DbModels;
using System.Data.Entity;

namespace IdentBase.Context
{
    public class IdentContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public IdentContext() : base("IdentConnection") { }
    }
}
