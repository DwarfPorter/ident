﻿using IdentBase.DbModels;
using System;
using System.Threading;

namespace IdentBase.Context
{
    internal class PersonInitializer
    {
        internal Person Person { get; private set; }
        internal Employee Employee { get; private set; }
        internal Patient Patient { get; private set; }

        private static int Id = 0;

        private static Random rnd = new Random();
        private PersonBuilder personBuilder = new PersonBuilder();

        internal void Build()
        {
            Interlocked.Increment(ref Id);
            Person = BuildPerson();

            if (RndNextBool())
            {
                Employee = BuildEmployee();
                if (RndNextBool())
                {
                    Patient = BuildPatient();
                }
            }
            else
            {
                Patient = BuildPatient();
            }
        }

        private Person BuildPerson()
        {
            return new Person()
            {
                Id = Id,
                Name = personBuilder.BuildName(),
                Surname = personBuilder.BuildSurname(),
                ParentalName = personBuilder.BuildParentalname(),
                Phone = personBuilder.BuildPhone()
            };
        }
        private Employee BuildEmployee()
        {
            return new Employee()
            {
                Id = Id,
                Inn = personBuilder.BuildInn()
            };
        }
        private Patient BuildPatient()
        {
            return new Patient()
            {
                Id = Id,
                NumberMedcard = personBuilder.BuildNumberMedcard()
            };
        }

        private bool RndNextBool()
        {
            return rnd.Next(2) == 0;
        }
    }
}
