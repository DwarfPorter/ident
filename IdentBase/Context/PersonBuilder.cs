﻿using IdentBase.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentBase.Context
{
    internal class PersonBuilder
    {

        private string[] name = { "Василий", "Иван", "Федор", "Семен", "Петр" };
        private string[] surname = { "Васильев", "Иванов", "Федоров", "Семенов", "Петров" };
        private string[] parentalname = { "Васильевич", "Иванович", "Федорович", "Семенович", "Петрович" };

        private static Random rnd = new Random();

        internal string BuildName()
        {
            return name[rnd.Next(name.Length)];
        }

        internal string BuildSurname()
        {
            return surname[rnd.Next(surname.Length)];
        }

        internal string BuildParentalname()
        {
            return parentalname[rnd.Next(parentalname.Length)];
        }

        internal string BuildInn()
        {
            return string.Format("78{0}2806", rnd.Next(1000, 9999));
        }

        internal string BuildPhone()
        {
            return string.Format("(812)-{0}-{1}-{2}", rnd.Next(100, 999), rnd.Next(10, 99), rnd.Next(10, 99));
        }

        internal string BuildNumberMedcard()
        {
            return string.Format("MC{0}", rnd.Next(1000));
        }
    }
}
