﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentBase.DbModels
{
    public class Employee
    {
        [Key]
        [ForeignKey("Person")]
        public int Id { get; set; }
        public string Inn { get; set; }
        public virtual Person Person { get; set; }
    }
}
