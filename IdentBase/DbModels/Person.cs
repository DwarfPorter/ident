﻿using System.ComponentModel.DataAnnotations;

namespace IdentBase.DbModels
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ParentalName { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
    }
}
