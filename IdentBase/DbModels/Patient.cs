﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentBase.DbModels
{
    public class Patient
    {
        [Key]
        [ForeignKey("Person")]
        public int Id { get; set; }
        public string NumberMedcard { get; set; }
        public virtual Person Person { get; set; }
    }
}
