﻿using IdentBase.Context;
using NUnit.Framework;

namespace IdentBaseTest
{
    [TestFixture]
    public class PersonBuilderTest
    {

        private PersonBuilder personBuilder = new PersonBuilder();

        [Test]
        public void BuildNameTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildName());
        }

        [Test]
        public void BuildSurnameTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildSurname());
        }

        [Test]
        public void BuildParentalameTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildParentalname());
        }

        [Test]
        public void BuilInnTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildInn());
        }

        [Test]
        public void BuilPhoneTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildPhone());
        }

        [Test]
        public void BuilNumberMedcardTest()
        {
            Assert.IsNotEmpty(personBuilder.BuildNumberMedcard());
        }
    }
}
